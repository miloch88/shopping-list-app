import { EventEmitter, Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Recipe } from "./recipe.model";

@Injectable()
export class RecipeService{
  recipeSelected = new EventEmitter<Recipe>();
  
  private recipes: Recipe[] = [
    new Recipe('A Breakfast', 
               'A sandwich and vegetables for breakfast', 
               'https://cdn.pixabay.com/photo/2018/07/29/22/39/bread-3571266_960_720.jpg',
               [  new Ingredient("Bread",1),
                  new Ingredient("Cheese", 5),
                  new Ingredient("Potato", 2)]),
    new Recipe('A Dinner', 
               'A hamburger for dinner', 
               'https://cdn.pixabay.com/photo/2014/10/19/20/59/hamburger-494706_960_720.jpg',
               [  new Ingredient("Buns",2),
                  new Ingredient("Meat", 1)]),
  ];

  constructor(private shoppingListService : ShoppingListService) {}

  getRecipes(){
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]){
    this.shoppingListService.addIngredients(ingredients);
  }
}